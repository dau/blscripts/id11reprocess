# Usage:
#
# On lid11control:
#
#  conda activate ewoksworker
#  python id11applywf.py
#

import os
import h5py
import logging
from glob import glob
from ewoks import execute_graph

logging.basicConfig(level=logging.INFO)


def applywf(raw_datasets, counters, workflow, results):
    detector = counters[0]
    for filename in glob(raw_datasets):
        collection_dataset, collection, scans = parse_dataset_path(filename, counters)
        if not scans:
            continue
        for scan in scans:
            output_root_dir = os.path.join(results, collection, collection_dataset)
            output_filename = os.path.join(output_root_dir, f"{collection_dataset}.h5")
            output_filename_scan = os.path.join(
                output_root_dir, f"{collection_dataset}_{scan}.h5"
            )
            convert_destination = os.path.join(
                output_root_dir, f"{collection_dataset}_{scan}_{detector}.json"
            )

            inputs = [
                {
                    "task_identifier": "IntegrateBlissScan",
                    "name": "filename",
                    "value": filename,
                },
                {
                    "task_identifier": "IntegrateBlissScan",
                    "name": "scan",
                    "value": scan,
                },
                {
                    "task_identifier": "IntegrateBlissScan",
                    "name": "output_filename",
                    "value": output_filename,
                },
                {
                    "task_identifier": "IntegrateBlissScan",
                    "name": "scan_memory_url",
                    "value": "",
                },
                {
                    "task_identifier": "PdfGetXSaveAscii",
                    "name": "filename",
                    "value": output_filename_scan,
                },
                {
                    "task_identifier": "PdfGetXSaveNexus",
                    "name": "output_filename",
                    "value": output_filename,
                },
                {"task_identifier": "PdfGetXSaveNexus", "name": "scan", "value": scan},
            ]

            try:
                _ = execute_graph(
                    workflow, inputs=inputs, convert_destination=convert_destination
                )
            except Exception:
                pass


def parse_dataset_path(filename, counters):
    collection_dataset = os.path.basename(os.path.dirname(filename))
    collection = os.path.basename(os.path.dirname(os.path.dirname(workflow)))

    scans = set()
    with h5py.File(filename, mode="r", locking=False) as nxroot:
        for name in nxroot:
            if all(f"{name}/measurement/{ctr}" in nxroot for ctr in counters):
                scans.add(int(float(name)))

    return collection_dataset, collection, sorted(scans)


if __name__ == "__main__":
    raw_datasets = "/data/visitor/ch6894/id11/20240411/RAW_DATA/CeO2/*/*.h5"
    counters = ["frelon6", "fpico1"]

    workflow = "/data/visitor/ch6894/id11/20240411/PROCESSED_DATA/Gd2O3/Gd2O3_750C/Gd2O3_750C_10_frelon6.json"
    results = "/tmp/test"

    applywf(raw_datasets, counters, workflow, results)
